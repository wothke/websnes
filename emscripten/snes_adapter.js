/*
 snes_adapter.js: Adapts GME backend to generic WebAudio/ScriptProcessor player.

 version 1.1

 	Copyright (C) 2018-2023 Juergen Wothke

 LICENSE

 GNU LESSER GENERAL PUBLIC LICENSE Version 2.1, February 1999 (see separate license.txt in gme folder).
*/
class SNESBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor()
	{
		super(backend_SNES.Module, 2, new SimpleFileMapper(backend_SNES.Module));

		this.ensureReadyNotification();
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);
		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), -999, false);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let id = (options && options.track) ? options.track : -1;	// by default do not set track
		return this.Module.ccall('emu_set_subsong', 'number', ['number'], [id]);
	}

	getSongInfoMeta()
	{
		return {
			title: String,
			artist: String,
			game: String,
			comment: String,
			copyright: String,
			dumper: String,
			system: String,
			tracks: String,
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;
		
		let numAttr = 8;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

		result.title = this.Module.Pointer_stringify(array[0]);
		result.artist = this.Module.Pointer_stringify(array[1]);
		result.game = this.Module.Pointer_stringify(array[2]);
		result.comment = this.Module.Pointer_stringify(array[3]);
		result.copyright = this.Module.Pointer_stringify(array[4]);
		result.dumper = this.Module.Pointer_stringify(array[5]);
		result.system = this.Module.Pointer_stringify(array[6]);
		result.tracks = this.Module.Pointer_stringify(array[7]);
	}
};

