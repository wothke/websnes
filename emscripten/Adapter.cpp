/*
* This file adapts Game_Music_Emu to the interface expected by my generic JavaScript player.
*
* Copyright (C) 2018-2023 Juergen Wothke
*
*
* Credits:
*
* The project is based on: https://bitbucket.org/mpyne/game-music-emu (by Shay Green, et al.)
* zlib (C) 1995-2005 Jean-loup Gailly and Mark Adler
*
* Notes:
*
* Only a small subset of the emulators available in GME are actually activated/used here, i.e. AY, GYM & SPC support.
* The reason is that for the other systems I already have other better emulators and there is no point in
* bloating this lib with unused stuff (this is also true for AY but dependency wise it seems to be required
* anyway). I added support for packed GYM (see hack below).
*
* License:
*
* The GME plugin is licensed using GNU LESSER GENERAL PUBLIC LICENSE Version 2.1, February 1999 (see license.txt in gme folder)
*/

#include <emscripten.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h> // memcpy

#include <iostream>
#include <fstream>

#include "gme/gme.h"
#include "Gym_Emu.h"

#include "zlib.h"

#include "MetaInfoHelper.h"

using emsutil::MetaInfoHelper;



#define CHANNELS 2
#define BYTES_PER_SAMPLE 2
#define SAMPLE_BUF_SIZE	1024


namespace gme {

class Adapter {
public:
	Adapter() : _sampleRate(0), _samplesAvailable(0), _isReady(false), _unpacked(0)
	{
	}

	void teardown()
	{
		_isReady = false;

		if (_trackInfo) { gme_free_info( _trackInfo ); _trackInfo = 0; }
		if (_emu) { gme_delete( _emu ); _emu = 0; }

		MetaInfoHelper::getInstance()->clear();
	}

	int32_t loadFile(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
	{
		// fixme: audioBufSize, scopesEnabled support not implemented

		_sampleRate = sampleRate;

		// LOL GME's GYM impl doesn't support packed files.. not much use without packed file support!
		if ( memcmp( inBuffer, "GYMX", 4 ) == 0 )
		{
			// seems to be easier to unpack here than to fix the emu
			// A 32-bit unsigned integer in little-endian format denoting how large the
			//             remainder of the data is if it's GZipped;
			unsigned long unpackedSize = getLE((uint8_t*)&((Gym_Emu::header_t const*) inBuffer)->packed[0]);
			if (unpackedSize != 0 )
			{
				// Packed GYM file
				if (_unpacked) free(_unpacked);	// limit the garbage

				_unpacked= (uint8_t *)malloc(Gym_Emu::header_size+unpackedSize);
				memcpy(_unpacked, inBuffer, Gym_Emu::header_size);	// header
				*((uint32_t*)(((Gym_Emu::header_t const*) _unpacked)->packed))= 0;	// mark as _unpacked

				if (Z_OK != uncompress(_unpacked+Gym_Emu::header_size, &unpackedSize,
								(const uint8_t*)inBuffer+Gym_Emu::header_size, inBufSize-Gym_Emu::header_size))
				{
					fprintf(stderr, "ERROR uncompressing\n");
				}
				inBuffer = (void*)_unpacked;
				inBufSize = Gym_Emu::header_size+unpackedSize;
			}
		}

		if (handleError(gme_open_data( inBuffer, inBufSize,&_emu, _sampleRate  ) ))
		{
			return 1;
		}
		return 0;
	}

	int32_t setSubsong(int32_t track)
	{
		if (track < 0)
		{
			track = 0;
		}
		if (track >= trackCount())
		{
			track = trackCount() - 1;
		}

		int32_t ret = updateInfo(track);
		_isReady = true;
		return ret;
	}

	void seekPosition(int32_t ms)
	{
		if (_isReady) gme_seek( _emu, ms );
	}

	int getMaxPosition()
	{
		return _isReady ? _trackInfo->length : -1;
	}

	int getCurrentPosition()
	{
		return _isReady ? gme_tell( _emu) : -1;
	}

	int32_t genSamples()
	{
		if (handleError( gme_play( _emu, SAMPLE_BUF_SIZE, (int16_t *)_sampleBuffer ) ))
		{
			return 1;
		}
		if (trackEnded())
		{
			_samplesAvailable = 0;
			return 1;
		}
		_samplesAvailable = SAMPLE_BUF_SIZE>>1;
		return 0;
	}

	char* getSampleBuffer()
	{
		return (char*)_sampleBuffer;
	}

	int32_t getSampleBufferLength()
	{
		return _samplesAvailable;
	}

	int getSampleRate()
	{
		return _sampleRate;
	}
private:
	std::string trim(const std::string& str)
	{
		size_t first = str.find_first_not_of(' ');
		if (std::string::npos == first)
		{
			return str;
		}
		size_t last = str.find_last_not_of(' ');
		return str.substr(first, (last - first + 1));
	}

	uint32_t getLE(uint8_t *buf)
	{
		return buf[0] + (buf[1]<<8) + (buf[2]<<16) + (buf[3]<<24);
	}

	const char* handleError( const char* str )
	{
		if ( str )
		{
			fprintf(stderr,  "Error: %s\n", str );
		}
		return str;
	}

	bool trackEnded()
	{
		return _emu ? gme_track_ended( _emu ) : false;
	}

	int32_t trackCount()
	{
		return _emu ? gme_track_count( _emu ) : 1;
	}

	int32_t updateInfo(int32_t track)
	{
		if (_trackInfo) gme_free_info( _trackInfo );

		if (handleError( gme_track_info( _emu, &_trackInfo, track ) ))
			return 1;

		if (handleError( gme_start_track( _emu, track ) ))
			return 1;

		// Calculate track length
		if ( _trackInfo->length <= 0 )
		{
			_trackInfo->length = _trackInfo->intro_length + _trackInfo->loop_length * 2;
		}
		if ( _trackInfo->length <= 0 )
		{
			_trackInfo->length = (int32_t) (2.5 * 60 * 1000);
		}
		gme_set_fade( _emu, _trackInfo->length );


		MetaInfoHelper *info = MetaInfoHelper::getInstance();
		info->setText(0, _trackInfo->song, "");
		info->setText(1, _trackInfo->author, "");
		info->setText(2, _trackInfo->game, "");
		info->setText(3, _trackInfo->comment, "");
		info->setText(4, _trackInfo->copyright, "");
		info->setText(5, _trackInfo->dumper, "");
		info->setText(6, _trackInfo->system, "");

		char tmp[3];
		snprintf(tmp, 3, "%d", trackCount());
		info->setText(7, tmp, "");

		return 0;
	}
private:
	// playback
	int _sampleRate;
	int16_t _sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];
	int32_t _samplesAvailable;
	bool _isReady ;

	// GME stuff
	Music_Emu* _emu;
	gme_info_t* _trackInfo;
	uint8_t * _unpacked;
};

};

gme::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown()) 							{ _adapter.teardown(); }
EMBIND(int32_t, emu_get_sample_rate()) 					{ return _adapter.getSampleRate(); }
EMBIND(int32_t, emu_set_subsong(int32_t track)) 		{ return _adapter.setSubsong(track); }
EMBIND(const char**, emu_get_track_info()) 				{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(char*, emu_get_audio_buffer()) 					{ return _adapter.getSampleBuffer(); }
EMBIND(int32_t, emu_get_audio_buffer_length()) 			{ return _adapter.getSampleBufferLength(); }
EMBIND(int32_t, emu_compute_audio_samples()) 			{ return _adapter.genSamples(); }
EMBIND(void, emu_seek_position(int32_t ms)) 			{ _adapter.seekPosition(ms); }
EMBIND(int, emu_get_current_position()) 				{ return _adapter.getCurrentPosition(); }
EMBIND(int, emu_get_max_position()) 					{ return _adapter.getMaxPosition(); }
