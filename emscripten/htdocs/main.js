let songs = [
		"Nintendo SPC/Richard Joseph/Super James Pond/super james pond - codename robocod.spc",		
		"Nintendo SPC/David Whittaker/Batman - Revenge of the Joker/01 - main theme.spc",		
		"Nintendo SPC/Allister Brimble/Street Racer/01 - title.spc",		
		"Nintendo SPC/Barry Leitch/Lethal Weapon/lethal weapon - ending.spc",		
	];

class SNESDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo, doGetScrapedInfo)
	{
		super(doGetSongInfo);

		this.getScrapedInfo = doGetScrapedInfo;
	}

		getDisplayTitle() 		{ return "webSNES";}
		getDisplaySubtitle() 	{ return "Super Nintendo music";}
		getDisplayLine1() { return this.getSongInfo().title; }
		getDisplayLine2() { return this.getSongInfo().artist+ (this.getSongInfo().artist.length?" ":"") + this.getSongInfo().game; }
		getDisplayLine3() { return this.getSongInfo().copyright; }
};

class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;

		this._scrapedInfo = {};
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	_getScrapedSongInfo()
	{
		return this._scrapedInfo;
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new SNESBackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let arr = someSong.split(";");					
					let track = arr.length > 1 ? parseInt(arr[1]) : -1;
					let timeout = arr.length > 2 ? parseInt(arr[2]) : -1;
					someSong = arr[0];

					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;
				
					let options = {};
					options.track = isNaN(track) ? -1 : track;
					options.timeout = isNaN(timeout) ? -1 : timeout;
					
					return [someSong, options];
				}.bind(this);

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new SNESDisplayAccessor(function(){return this._playerWidget.getSongInfo();}.bind(this),
								this._getScrapedSongInfo.bind(this) ), [0x505050,0xffffff,0x404040,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}